<?php
/**
 * @category Bitbull
 * @package  Bitbull_MagentoCollectionBlock
 * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
 */

class Bitbull_MagentoCollectionBlock_Block_Abstract extends Mage_Catalog_Block_Product_Abstract{

    /**
     * limit of product to show, the default value is 8
     * @var int
     */
    protected $_limit = 8;
    protected $_columnCount = 8;
    protected $_tagId;
    protected $_random= false;
    protected $_collection;
    protected $_select= array();

    /** used for custom collection order. default is entity_id, desc */
    protected $_sortOrder = "entity_id";
    protected $_sortDirection = "DESC";


    /**
     * Set number of product to show
     * @param  int $count
     * @return Bitbull_MagentoCollectionBlock_Block_Custom
     */
    public function setColumnCount($count){
        $this->_columnCount= $count;
        return $this;
    }

    /**
     * number of columns to show
     * @return int
     */
    public function getColumnCount(){
        return $this->_columnCount;
    }

    /**
     * set limit in collection
     * @param $count
     * @return $this
     */
    public function setLimit($count){
        $this->_limit= $count;
        return $this;
    }

    /**
     * Set the id to add in the view
     * @param $id
     * @return $this
     */
    public function setTagId($id){
        $this->_tagId = $id;
        return $this;
    }

    /**
     * Get the tag id to add in view
     * @return mixed
     */
    public function getTagId(){
        return $this->_tagId;
    }


    /**
     * Set order random in collection
     * @return Bitbull_MagentoCollectionBlock_Block_Custom
     */
    public function enableRandomProduct(){
        $this->_random=true;
        return $this;
    }

    public function addAttributeToSelect($name){
        $this->_select[]= $name;
        return $this;
    }

    /**
     * Set custom order to collection
     * default is by entity_id, desc
     *
     * @param $sortOrder
     * @param $sortDirection
     *
     * @return $this
     */
    public function setOrder($sortOrder, $sortDirection)
    {
        if (!is_null($sortOrder)) {
            $this->_sortOrder = $sortOrder;
        }
        if (!is_null($sortDirection)) {
            $this->_sortDirection = $sortDirection;
        }
        return $this;
    }


    public function getProductCollection(){

        if($this->_collection==null){

            /** @var Bitbull_MagentoCollectionBlock_Helper_Data $helper */
            $helper = Mage::helper('bitbull_magentocollectionblock/data');

            /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
            $collection =  Mage::getResourceModel('catalog/product_collection');
            //remove non visibile product
            $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());


            //add select attribute in collection
            if(count($this->_select)){
                $collection =$helper->addAttributesToSelect($collection, $this->_select);
            }

            //check if is request random product
            if($this->_random){
                //set random order
                $collection->getSelect()->order(new Zend_Db_Expr('RAND()'));
            } else {
                $collection->addAttributeToSort($this->_sortOrder, $this->_sortDirection);
            }

            $this->_addProductAttributesAndPrices($collection)
                ->addStoreFilter()
                //add limit to collection
                ->setPageSize($this->_limit)
                ->setCurPage(1);

            if(Mage::getStoreConfig('magentocollectionblock/config/enabled_visibility')){
                // Occorre che siano recuperati anche i prodotti visibili solo in ricerca
                $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInSiteIds());
            }

            $this->_collection = $collection;
        }
        return $this->_collection;
    }

} 