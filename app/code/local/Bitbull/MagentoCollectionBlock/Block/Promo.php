<?php
/**
 * @category Bitbull
 * @package  Bitbull_MagentoCollectionBlock
 * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
 */

class Bitbull_MagentoCollectionBlock_Block_Promo extends Bitbull_MagentoCollectionBlock_Block_Abstract{

    public function getProductCollection()
    {
        $collection =   parent::getProductCollection();

        //add filter only on the final price to find product in promo (now)
        $collection
            ->getSelect()
            ->where('price_index.final_price < price_index.price');

        return $collection;
    }
} 