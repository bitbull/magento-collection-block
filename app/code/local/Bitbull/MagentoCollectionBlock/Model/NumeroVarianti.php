<?php
class Bitbull_MagentoCollectionBlock_Model_NumeroVarianti extends Mage_Core_Model_Abstract
{
    private $_select;

    public function __construct($attributeId){
        $select = Mage::getSingleton('core/resource')->getConnection('core_read')->select();
        $select->from('catalog_product_entity_varchar');
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->columns(array('TotalRecords' => new Zend_Db_Expr('COUNT(*)')));
        $select->where('attribute_id = ? ',$attributeId);
        $select->group('value');
        $this->_select = $select;

    }

    public function getNumeroVariantiByValue($value){
        $db = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select = $this->_select;
        $select->where('value = ? ',$value);
        $select->query();
        return $db->fetchOne($select,'TotalRecords');
    }
}
?>
 
